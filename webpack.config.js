const path = require("path");
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
	mode: "development",
	entry: ["./app/index.js"],
	output: {
		filename: "js/main.js",
		path: path.resolve(__dirname, "public/")
	},
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{
						loader: "vue-style-loader",
					},
					{
						loader: "css-loader",

					},
					{
						loader: "sass-loader",
					}
				]
			},
			{
				test: /\.vue$/,
				use: {
					loader: "vue-loader",
				}
			},
			{
				test: /\.(png|jpe?g|gif)$/i,
				use: {
					loader: 'file-loader',
					options: {
						name: '[path][name].[ext]',
						outputPath: 'images/'
					}
				}
			}
		]
	},
	plugins: [
	  	new VueLoaderPlugin()
	]
};
